﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nUnit_testausta
{
    [TestFixture]
    class Yksikkotestit
    {
        [TestCase]
        public void Add()
        {
            Laskin laskin = new Laskin();
            Assert.AreEqual(11, laskin.add(5, 6));
        }

        [TestCase]
        public void Add2()
        {
            Laskin laskin = new Laskin();
            Assert.AreEqual(2, laskin.add2(1, 1));
        }

        [TestCase]
        public void Subtraction()
        {
            Laskin laskin = new Laskin();
            Assert.AreEqual(6, laskin.subtraction(10, 4));
        }

        [TestCase]
        public void Multiplication()
        {
            Laskin laskin = new Laskin();
            Assert.AreEqual(25, laskin.multiplication(5, 5));
        }

        [TestCase]
        public void Division()
        {
            Laskin laskin = new Laskin();
            Assert.AreEqual("4", laskin.division(8, 2));
        }

        [TestCase]
        public void DivisionZero()
        {
            Laskin laskin = new Laskin();
            Assert.AreEqual("nollalla ei voi jakaa", laskin.division(7, 0));
        }

        [TestCase]
        public void DivisionDecimal()
        {
            Laskin laskin = new Laskin();
            Assert.AreEqual("1.5", laskin.division(3, 2));
        }

        [TestCase]
        public void power()
        {
            Laskin laskin = new Laskin();
            Assert.AreEqual(3125, laskin.power(5, 5));
        }

        [TestCase]
        public void reminder()
        {
            Laskin laskin = new Laskin();
            Assert.AreEqual(2, laskin.remainder(5, 3));
        }
    }
}
