﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nUnit_testausta
{
    class Laskin
    {
        public int add(int a, int b)
        {
            return a + b;
        }

        public int add2(int a, int b)
        {
            return a + b + 1;
        }

        public int subtraction(int a, int b)
        {
            return a - b;
        }

        public int multiplication(int a, int b)
        {
            return a * b;
        }

        public string division(int a, int b)
        {
            if (b==0)
            {
                return "nollalla ei voi jakaa"; 
            }

            double tulos = (double)a / b;
            return tulos.ToString();
        }

        public int power(int a, int b)
        {
            int tulos=a;
            for (int i=1; i<b; i++)
            {
                tulos = tulos * a;
            }

            return tulos;
        }

        public int remainder(int a, int b)
        {
            return a % b;
        }

    }
}
