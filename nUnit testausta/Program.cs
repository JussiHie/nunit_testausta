﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nUnit_testausta
{
    class Program
    {
        static void Main(string[] args)
        {
            Laskin laskin = new Laskin();

            Console.WriteLine("Lisätääs tännekkin 'testejä'");
            Console.WriteLine("5+5=" + laskin.add(5, 5));
            Console.WriteLine("10-4=" + laskin.subtraction(10, 4));
            Console.WriteLine("3*4=" + laskin.multiplication(3, 4));
            Console.WriteLine("1+1=" + laskin.add2(1, 1) + " ???");
            Console.WriteLine("6/2=" + laskin.division(6, 2));
            Console.WriteLine("3/0=" + laskin.division(3, 0));
            Console.WriteLine("3/2=" + laskin.division(3, 2));
            Console.WriteLine("8^4=" + laskin.power(8, 4));
            Console.WriteLine("5%3=" + laskin.remainder(5, 3));
        }
    }
}
